//menusPrincipaux.h
#ifndef MENUSPRINCIPAUX_HH_
#define MENUSPRINCIPAUX_HH_

#include "menus.hh"

void menuRacedriver();
void menuJeu();

void menuChargementPartie();
void menuCreationPartie();
void menuSuppressionPartie();

void menuApropos(); //Menu promotionnel

#endif /* !MENUSPRINCIPAUX_HH_ */