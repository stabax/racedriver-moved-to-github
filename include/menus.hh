
//menus.h

#ifndef menus
#define menus

#include "sousMenus.hh"
#include"Game.hh"

void menuCourse(); //Menu de selection du type de course
void menuGarage(); //Menu de gestion des vehicules
void menuConcessionaire(); // Achat/vente de vehicules
void menuStats();
void menuSauvegarde();
void menuOptions();


#endif
